﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Models.Request;
using Avira.MarketingResearch.Models.Response;
using Avira.MarketingResearch.Models.Entities;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Service.CoreInterfaces;
using Avira.MarketingResearch.Repository.UnitOfWork;
namespace Avira.MarketingResearch.Service.Implementations
{
    public class CountryService : ICountryService
    {
        private readonly ICountryRepository _countryRepository;

        public CountryService(ICountryRepository countryRepository)
        {
            _countryRepository = countryRepository;
        }

        public async Task<CountriesResponse> GetAsync(Guid Id)
        {
            CountriesResponse countryResponse = new CountriesResponse();
            var country = await _countryRepository.GetAsync(Id);

            if (country == null)
            {
                countryResponse.Message = "Country not found.";
            }
            else
            {
                countryResponse.countries.Add(country);
            }

            return countryResponse;
        }

        public async Task<CountriesResponse> GetAllAsync()
        {
            //TODO: Paging...

            CountriesResponse countriesReponse = new CountriesResponse();
            IEnumerable<Countries> country = await _countryRepository.GetAllAsync();

            if (country.ToList().Count == 0)
            {
                countriesReponse.Message = "Country not found.";
            }
            else
            {
                countriesReponse.countries.AddRange(country);
            }

            return countriesReponse;
        }

        public async Task AddAsync(CountriesRequest countryRequest)
        {
            Countries countries = new Countries()
            {
                CountryName = countryRequest.CountryName,
                CreatedOn = System.DateTime.UtcNow
               
            };

            await _countryRepository.AddAsync(countries);
        }

        public void Add(CountriesRequest countriesRequest)
        {
            Countries country = new Countries()
            {
                CountryName = countriesRequest.CountryName,
                 
                CreatedOn = System.DateTime.UtcNow
            };
            _countryRepository.Insert(country);
            //var unitofWork = new AviraUnitOfWork();
            //unitofWork.BaseRepository<Product>().Insert(product);
            //unitofWork.Commit();              
        }
    }
}
