using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Avira.MarketingResearch.Models.Response;
using Avira.MarketingResearch.Models.Request;

namespace Avira.MarketingResearch.Service.CoreInterfaces
{
    public interface IProductService
    {
        Task<ProductResponse> GetAsync(long id);
        Task<ProductResponse> GetAllAsync();
        Task AddAsync(ProductRequest productRequest);
        void Add(ProductRequest productRequest);
    }
}