﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Avira.MarketingResearch.Models.Response;
using Avira.MarketingResearch.Models.Request;



namespace Avira.MarketingResearch.Service.CoreInterfaces
{
    public interface ICountryService
    {
        Task<CountriesResponse> GetAsync(Guid Id);
        Task<CountriesResponse> GetAllAsync();
        Task AddAsync(CountriesRequest productRequest);
        void Add(CountriesRequest productRequest);
    }
}
