﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Avira.MarketingResearch.Models.Entities;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Implementations;

namespace Avira.MarketingResearch.Repository.UnitOfWork
{
    public class AviraUnitOfWork : IDisposable
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        //https://www.danylkoweb.com/Blog/a-better-entity-framework-unit-of-work-pattern-DD
        private string _connectionString = "";
        public AviraUnitOfWork()
        {

            _connection = new SqlConnection(_connectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }


        public Dictionary<Type, object> repositories = new Dictionary<Type, object>();

        //public IBaseRepository<T> BaseRepository<T>() where T : IAggregateRoot
        //{
        //    if (repositories.Keys.Contains(typeof(T)) == true)
        //    {
        //        return repositories[typeof(T)] as IBaseRepository<T>;
        //    }
        //    IBaseRepository<T> repo = new BaseRepository<T>(_connection);
        //    repositories.Add(typeof(T), repo);
        //    return repo;
        //}

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
            }
        }

        public void Dispose()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }

            if (_connection != null)
            {
                _connection.Dispose();
                _connection = null;
            }
        }


    }
}
