﻿using Avira.MarketingResearch.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper.Contrib.Extensions;
using Avira.MarketingResearch.Repository.CoreInterfaces;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class BaseRepository<T> : IBaseRepository<T> where T : IAggregateRoot
    {


        private readonly IDbConnection _connection;
        private readonly string _connectionString;
       

        public BaseRepository()
        {             
            _connectionString = "Server=TitanDev\\MSSQLSERVER01;Database=Avira;Trusted_Connection=True;MultipleActiveResultSets=true";
            _connection= new SqlConnection(_connectionString);
        }

        public bool Delete(T obj)
        {
           return _connection.Delete(obj);
        }

        public bool Delete(IEnumerable<T> list)
        {
            throw new NotImplementedException();
        }

        public bool DeleteAll()
        {
            return _connection.DeleteAll<T>();
        }

        public T Get(Guid id)
        {
            return _connection.Get<T>(id);
        }

        public T Get(long id)
        {
            return _connection.Get<T>(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _connection.GetAll<T>();
        }

        public long Insert(T obj)
        {
            return _connection.Insert(obj);
        }

        public int Insert(IEnumerable<T> list)
        {
            throw new NotImplementedException();
        }

        public bool Update(T obj)
        {
            return _connection.Update(obj);
        }

        public bool Update(IEnumerable<T> list)
        {
            throw new NotImplementedException();
        }

        
    }
}

