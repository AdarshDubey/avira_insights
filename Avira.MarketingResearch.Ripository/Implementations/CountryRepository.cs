﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Avira.MarketingResearch.Models.Entities;
using Dapper;
using Avira.MarketingResearch.Repository.CoreInterfaces;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class CountryRepository: BaseRepository<Countries>, ICountryRepository
    {
        private readonly string _connectionString;
        private IDbConnection _connection { get { return new SqlConnection(_connectionString); } }

        public CountryRepository()
        {
            // TODO: It will be refactored...
            //_connectionString = "Server=DESKTOP-VHIT849;Database=Test;User ID=sa;Password=Soft$1234; Integrated Security=false;Trusted_Connection=True;MultipleActiveResultSets=true";
            _connectionString = "Server=67.180.203.108;Database=Avira;User ID=AviraUser;Password=43jK7DKST8;Trusted_Connection=False;";
        }

        public async Task<Countries> GetAsync(Guid Id)
        {
            using (IDbConnection dbConnection = _connection)
            {
               

                var countries = await dbConnection.QueryFirstOrDefaultAsync<Countries>("[dbo].[SApp_GetCountrybyID]", new { @CountryID= Id},
                       commandType: CommandType.StoredProcedure);
                return countries;
            }
        }
        public async Task<IEnumerable<Countries>> GetAllAsync()
        {
            //TODO: Paging...
            using (IDbConnection dbConnection = _connection)
            {
                var countries = await dbConnection.QueryAsync<Countries>("GetAllCountries",                        
                        commandType: CommandType.StoredProcedure);
                

                return countries;
            }
        }

        public async Task AddAsync(Countries countries)
        {
            using (IDbConnection dbConnection = _connection)
            {
                //string query = @"INSERT INTO [dbo].[Country] (

                //                [CategoryId],
                //                [Name],
                //                [Description],
                //                [Price],
                //                [CreatedDate]) VALUES (

                //                @CategoryId,
                //                @Name,
                //                @Description,
                //                @Price,
                //                @CreatedDate)";
                //var countries = await dbConnection.ad<Countries>("[dbo].[SApp_GetCountrybyID]", new { @CountryID = Id },
                await dbConnection.ExecuteAsync("[dbo].[SApp_GetCountrybyID]", new { @CountryID = countries.Id, @CountryName = countries.CountryName });
                    
               

            }
        }


    }
}
