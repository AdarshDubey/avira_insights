﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Avira.MarketingResearch.Models.Entities;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface ICountryRepository : IBaseRepository<Countries>
    {
        Task<Countries> GetAsync(Guid Id);
        Task<IEnumerable<Countries>> GetAllAsync();
        Task AddAsync(Countries countries);
    }
}
