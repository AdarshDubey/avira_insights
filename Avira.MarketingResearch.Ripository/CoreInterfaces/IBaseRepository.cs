﻿using Avira.MarketingResearch.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface IBaseRepository<T> where T : IAggregateRoot
    {
        T Get(Guid Id);
        T Get(long id);
        IEnumerable<T> GetAll();
        long Insert(T obj);
        int Insert(IEnumerable<T> list);
        bool Update(T obj);
        bool Update(IEnumerable<T> list);
        bool Delete(T obj);
        bool Delete(IEnumerable<T> list);
        bool DeleteAll();
    }

}
