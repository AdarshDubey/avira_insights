using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Avira.MarketingResearch.Models.Entities;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface IProductRepository : IBaseRepository<Product>
    {
        Task<Product> GetAsync(long id);
        Task<IEnumerable<Product>> GetAllAsync();
        Task AddAsync(Product product);
    }
}