﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Avira.MarketingResearch.Models.Request;
using Avira.MarketingResearch.Models.Response;
using Avira.MarketingResearch.Service.CoreInterfaces;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Cors;
using System.Diagnostics;

namespace Avira.MarketingResearch.WebMVC.Controllers
{
    [EnableCors("AllowOrigin")]
    public class HomeController : Controller
    {
        private readonly ICountryService _CountrySegment;

        public HomeController(ICountryService countrySegment)
        {
            _CountrySegment = countrySegment;
        }
        public IActionResult Index()
        {
          
            return View();
        }

        public IActionResult Company()
        {

            return View();
        }

        public IActionResult Component()
        {

            return View();
        }

        public IActionResult Region()
        {
            return View();
        }

        public IActionResult Service()
        {
            return View();
        }

        public IActionResult RawMaterial()
        {
            return View();
        }

        public IActionResult CountryRegionMap()
        {
            return View();
        }

        public IActionResult Industry()
        {
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new Models.ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> AddEditCountry(Guid Id)
        {
            

            var data= await _CountrySegment.GetAsync(Id);
            data.countries.Where(x => x.Id.Equals(Id)).FirstOrDefault();
            
            return View(data.countries.Where(x => x.Id.Equals(Id)).FirstOrDefault());
        }
        

    }
}
