﻿using Microsoft.AspNetCore.Mvc;

namespace Avira.WebMVC.Controllers
{
    internal class FilePathResult : ActionResult
    {
        private string v1;
        private string v2;

        public FilePathResult(string v1, string v2)
        {
            this.v1 = v1;
            this.v2 = v2;
        }
    }
}