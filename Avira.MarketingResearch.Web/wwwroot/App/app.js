﻿//(function () {
//    'use strict'
//    angular.module('example', [$scope]);
//})();

(function () {
    'use strict';
    var app = angular.module('example', []);
    app.controller('MainCtrl', CoreFunction)

    function CoreFunction() {
        var vm = this,
            employeeCollection = [],
            isEditing = false;

        // functions that are not attached to the view model 
        var add = function () {
            var newEmployee = {};

            if (!angular.equals({}, vm.employee)) {
                if (isEditing !== false) {
                    employeeCollection[isEditing] = vm.employee;
                    isEditing = false;
                } else {
                    newEmployee = vm.employee
                    employeeCollection.push(newEmployee);
                }

                vm.employees = employeeCollection;
                vm.employee = {};
            }
        },

            edit = function (editEmployee) {
                isEditing = employeeCollection.indexOf(editEmployee);
                vm.employee = angular.copy(editEmployee);
            },

            remove = function (removeEmployee) {
                var index = employeeCollection.indexOf(removeEmployee);
                employeeCollection.splice(index, 1);
                if (employeeCollection.length === 0) {
                    vm.employee = {};
                    vm.employees = undefined;
                }
            },

            reset = function () {
                vm.employee = {};
                isEditing = false;
            }

        // view model attached click handlers
        vm.addClickHandler = function () {
            add();
        }

        vm.editClickHandler = function (editEmployee) {
            edit(editEmployee);
        }

        vm.removeClickHandler = function (removeEmployee) {
            remove(removeEmployee);
        }

        vm.resetClickHandler = function () {
            reset();
        }
    }
}());//(function () {
//    'use strict'
//    angular.module('example', [$scope]);
//})();

(function () {
    'use strict';
    var app = angular.module('example', []);
    app.controller('MainCtrl', CoreFunction)

    function CoreFunction() {
        var vm = this,
            employeeCollection = [],
            isEditing = false;

        // functions that are not attached to the view model 
        var add = function () {
            var newEmployee = {};

            if (!angular.equals({}, vm.employee)) {
                if (isEditing !== false) {
                    employeeCollection[isEditing] = vm.employee;
                    isEditing = false;
                } else {
                    newEmployee = vm.employee
                    employeeCollection.push(newEmployee);
                }

                vm.employees = employeeCollection;
                vm.employee = {};
            }
        },

            edit = function (editEmployee) {
                isEditing = employeeCollection.indexOf(editEmployee);
                vm.employee = angular.copy(editEmployee);
            },

            remove = function (removeEmployee) {
                var index = employeeCollection.indexOf(removeEmployee);
                employeeCollection.splice(index, 1);
                if (employeeCollection.length === 0) {
                    vm.employee = {};
                    vm.employees = undefined;
                }
            },

            reset = function () {
                vm.employee = {};
                isEditing = false;
            }

        // view model attached click handlers
        vm.addClickHandler = function () {
            add();
        }

        vm.editClickHandler = function (editEmployee) {
            edit(editEmployee);
        }

        vm.removeClickHandler = function (removeEmployee) {
            remove(removeEmployee);
        }

        vm.resetClickHandler = function () {
            reset();
        }
    }
}());