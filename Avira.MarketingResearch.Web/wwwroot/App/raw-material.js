﻿
var popup, dataTable;
$(document).ready(function () {
    $.ajax({
        url: "https://localhost:44350/api/v1/rawmaterial",
        method: "GET",
        dataType: "JSON",
        success: function (data) {
            $("#data-table").DataTable({
                data: data.rawmaterial,
                "columns": [
                    { data: "Raw Material Name" },
                    { data: "Description" },
                    {
                        data: "id",
                        render: function (data) {
                            return "<a class='btn btn-default btn-xs btn-rounded p-l-10 p-r-10' onclick=ShowPopup('/Home/AddEditCountry/" + data + "')><i class='fa fa-cog'></i>  Edit</a><a class='btn btn-default btn-xs btn-rounded p-l-10 p-r-10' style='margin-left:5px' onclick=Delete(" + data + ")><i class='fa fa-trash'></i> Delete</a>";
                        }
                    }
                ],
                
                language: {
                    "emptyTable": "no data found."
                }
            });
        }
    });
});


function ShowPopup(url) {
    var formDiv = $('<div/>');
    $.get(url)
        .done(function (response) {
            formDiv.html(response);
            popup = formDiv.dialog({
                autoOpen: true,
                resizeable: false,
                width: 400,
                height: 300,
                title: 'Add or Edit Data',
                close: function () {
                    popup.dialog('destroy').remove();
                }
            });
        });
};


function SubmitAddEdit(form) {
    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        var data = $(form).serializeJSON();
        data = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            url: '/api/todo',
            data: data,
            contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    popup.dialog('close');
                    ShowMessage(data.message);
                    dataTable.ajax.reload();
                }
            }
        });

    }
    return false;
};

function Delete(id) {
    swal({
        title: "Are you sure want to Delete?",
        text: "You will not be able to restore the file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            type: 'DELETE',
            url: '/api/todo/' + id,
            success: function (data) {
                if (data.success) {
                    ShowMessage(data.message);
                    dataTable.ajax.reload();
                }
            }
        });
    });


};



