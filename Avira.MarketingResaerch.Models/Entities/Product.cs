using System;

namespace Avira.MarketingResearch.Models.Entities
{
    public class Product : IAggregateRoot
    {

        public long CategoryId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid Id { get; set; }
    }
}