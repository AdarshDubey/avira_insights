﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Models.Entities
{
    public class Countries:IAggregateRoot
    {
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Guid UserModifiedById { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public Guid UserDeletedById { get; set; }
        public Guid Id { get; set; }


    }
}
