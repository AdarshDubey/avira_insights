using System;
using System.Collections.Generic;
using Avira.MarketingResearch.Models.Entities;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Models.Response
{
    public class ProductResponse
    {
        public ProductResponse()
        {  
             Products = new List<Product>();
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        public List<Product> Products { get; set; }
    }
}