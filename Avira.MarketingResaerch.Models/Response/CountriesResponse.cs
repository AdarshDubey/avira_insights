﻿using System;
using System.Collections.Generic;
using Avira.MarketingResearch.Models.Entities;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Models.Response
{
    public class CountriesResponse
    {
        public CountriesResponse()
        {
            countries = new List<Countries>();
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        public List<Countries> countries { get; set; }

    }
}
