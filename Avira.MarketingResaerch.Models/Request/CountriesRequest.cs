﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Models.Request
{
   public class CountriesRequest
    {
        public string CountryName { get; set; }
    }
}
