﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Avira.MarketingResearch.Models.Request;
using Avira.MarketingResearch.Models.Response;
using Avira.MarketingResearch.Service.CoreInterfaces;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Cors;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [EnableCors("MyOrigin")]
    public class CountryController : Controller
    {
        private readonly ICountryService _CountrySegment;

        public CountryController(ICountryService countrySegment)
        {
            _CountrySegment = countrySegment;
        }
        // GET: api/v1/Country
        [HttpGet]
        public async Task<CountriesResponse> Get()
        {


            return await _CountrySegment.GetAllAsync();
          
        }

        // GET api/v1/Country/{id}  
        [HttpGet("{id}", Name = "Get")]
        public async Task<CountriesResponse> Get(Guid Id)
        {
            return await _CountrySegment.GetAsync(Id);
        }
        
        // POST: api/Country
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Country/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
