﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Avira.MarketingResearch.Models.Request;
using Avira.MarketingResearch.Models.Response;
using Avira.MarketingResearch.Service.CoreInterfaces;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    public class ProductsController : Controller
    {
        private readonly IProductService _productBusiness;

        public ProductsController(IProductService productBusiness)
        {
            _productBusiness = productBusiness;
        }

        // GET api/v1/products/{id}
        [HttpGet("{id}")]
        public async Task<ProductResponse> Get(long id)
        {
            return await _productBusiness.GetAsync(id);
        }

        // GET api/v1/products
        [HttpGet]
        public async Task<ProductResponse> Get()
        {
            return await _productBusiness.GetAllAsync();
        }

      

        //// POST api/v1/products
        //[ProducesResponseType(201)]
        //[HttpPost]
        //public async Task Post([FromBody]ProductRequest productRequest)
        //{
        //    await _productBusiness.AddAsync(productRequest);
        //}


        [ProducesResponseType(201)]
        [HttpPost]
        public void Post([FromBody]ProductRequest productRequest)
        {
            _productBusiness.Add(productRequest);
        }
    }
}